package com.dynamodb;


import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.local.embedded.DynamoDBEmbedded;
import com.amazonaws.services.dynamodbv2.local.main.ServerRunner;
import com.amazonaws.services.dynamodbv2.local.server.DynamoDBProxyServer;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.ListTablesResult;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.QueryRequest;
import org.apache.commons.cli.ParseException;

import java.util.Iterator;


public class App {

    private static AmazonDynamoDB client;

    // java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb

    public static void main(String[] args) {
        App app = new App();
        app.createBook();
        System.out.println("JSON document stored");
        Books books = app.getBook("1");
        System.out.println("Found document: " + books);
        app.crud();
        System.out.println(app.getBook("602"));

        //get from diff fields
    }


    static AmazonDynamoDB getClient() {
        String ACCESS_KEY = "key";
        String SECRET_KEY = "secret";
        String dBEndpoint = "http://localhost:8000";
        AmazonDynamoDBClient dbClient = new AmazonDynamoDBClient(new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY)).withEndpoint(dBEndpoint);

        System.out.println(dbClient.listTables());
        client = dbClient;

        return dbClient;
    }

    public static void createTables(Class c, String tableName, AmazonDynamoDB client, DynamoDBMapper mapper) {
        CreateTableRequest tableRequest = mapper.generateCreateTableRequest(c); // 1

        tableRequest.setProvisionedThroughput(new ProvisionedThroughput(1L, 1L)); // 2
        if (client.listTables().getTableNames().contains(tableName)) {
            client.deleteTable(tableName);
        }

        client.createTable(tableRequest); // 3
    }

    private void createBook() {
        AmazonDynamoDB client = getClient();
        DynamoDBMapper mapper = new DynamoDBMapper(client);
        createTables(Books.class, "Books", client, mapper);

        Books books = new Books("1", "Book1", "1234", "29.99");
        mapper.save(books);
    }

    private Books getBook(String id) {
        DynamoDBMapper mapper = new DynamoDBMapper(client);
        return mapper.load(Books.class, id);
    }

    private void crud() {
        Books books = new Books("601", "Books 601", "123466666", "129.99");
        DynamoDBMapper mapper = new DynamoDBMapper(client);
        mapper.save(books);

        Books itemRetrieved = mapper.load(Books.class, "601");
        System.out.println("Item retrieved:");
        System.out.println(itemRetrieved);
        Books books1 = new Books();
        books1.setId("602");
        books1.setIsbn("602602");
        books1.setName("602name");
        books1.setCost("33.33");

        mapper.save(books1);

        itemRetrieved.setIsbn("622-2222222222");
        itemRetrieved.setCost("111.11");
        mapper.save(itemRetrieved);
        System.out.println("Item updated:");
        System.out.println(itemRetrieved);

        DynamoDBMapperConfig config = DynamoDBMapperConfig.builder().withConsistentReads(DynamoDBMapperConfig.ConsistentReads.CONSISTENT).build();
        Books updatedItem = mapper.load(Books.class, "601", config);
        System.out.println("Retrieved the previously updated item:");
        System.out.println(updatedItem);

        Books item = new Books();
        //item.setName("602name");

        /*DynamoDB dynamoDB = new DynamoDB(client);
        Table table = dynamoDB.getTable("Books");

        QuerySpec spec = new QuerySpec()
                .withFilterExpression("V_name = :names")
                .withValueMap(new ValueMap()
                        .withString(":names", "602name"));

        ItemCollection<QueryOutcome> items = table.query(spec);
        Iterator<Item> iterator = items.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next().toJSONPretty());
        }*/

        // Delete the item.
        mapper.delete(updatedItem);

        // Try to retrieve deleted item.
        Books deletedItem = mapper.load(Books.class, updatedItem.getId(), config);
        if (deletedItem == null) {
            System.out.println("Done - Sample item is deleted.");
        }
    }
}
